#Example taken from https://cucumber.io/docs/guides/10-minute-tutorial/
# [acceptancetest id=AT0001 story=US0006]
Feature: Is it Friday yet?
  Everybody wants to know when it's Friday

  Scenario: Sunday isn't Friday
    Given today is Sunday
    When I ask whether it's Friday yet
    Then I should be told "Nope"