# Meeting Scheduler System

System requirements for a fictitious meeting scheduler system

## High-level requirements

The following requirements describes high level system requirements

### [requirement id=REQ0010]

The system shall allow scheduling of resources

## Detailed system requirements

The following requirements are derived from the above system level requirements

### [requirement id=REQ0011 parent=REQ0010]

The system must have the ability to schedule rooms and equipment.

### [requirement id=REQ0012 parent=REQ0010]

The system should be able to set attributes of a resource (e.g. room capacity, time of day, resource owner, who is authorized, or special features).
